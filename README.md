# Search in Hashicorp Vault KV

Searching in Vault first argument. Search is case insensetive. Wildcards are not supported but script is looking for partial entry of the argument.

### Required environment varialbes:
* VAULT_ADDR - Format is PROTOCOL+FQDN of vault instance;
* VAULT_NS - Vault namespace;
* VAULT_KV_PATH - name of KV where search will start from.

### Usage
* vs TEXT
