import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
     name='vault_search',
     version='0.2.5',
     scripts=['vs'],
     author="Maks Mikhalov",
     author_email="dn2tx.com@gmail.com",
     description="Utility that searching inside vault",
     long_description=long_description,
     long_description_content_type="text/markdown",
     url="https://gitlab.com/maksmikhalov/vault_search.git",
     packages=setuptools.find_packages(),
     install_requires=[
          'colorlog~=4.0.2',
          'requests~=2.23.0',
          'urllib3~=1.24.1'
      ],
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
 )

print('To use script add VAULT_ADDR, VAULT_NS and VAULT_KV_PATH to your ENV')
print('Usage:')
print('vs TEXT')
