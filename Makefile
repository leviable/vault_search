SHELL := /bin/bash

.PHONY: dependencies
dependencies:
	pip3 install -r ./requirements.txt

.PHONY: install 
install: build
	pip3 install .

.PHONY: build 
build: clean dependencies
	cp ./vault-search.py vs
	python3 setup.py bdist_wheel
	rm vs

.PHONY: push
push: build
	python3 -m twine upload -u $${PIP_USERNAME} -p $${PIP_PASSWORD} --repository-url https://upload.pypi.org/legacy/ dist/*

.PHONY: clean
clean:
	rm -rf build
	rm -rf dist
	rm -rf *.egg-info